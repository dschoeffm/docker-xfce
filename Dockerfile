FROM ubuntu:18.04

ENV DISPLAY=:1 \
    VNC_PORT=5901
EXPOSE $VNC_PORT

COPY install_base.sh install_chrome.sh install_firefox.sh /root/

# In the future, download the current chrome
ADD https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb /root/chrome.deb

RUN /root/install_base.sh
RUN /root/install_firefox.sh
RUN /root/install_chrome.sh

# This is the alternative, downloading it from the internet
ADD https://bintray.com/tigervnc/stable/download_file?file_path=tigervnc-1.9.0.x86_64.tar.gz /root/tigervnc.tar.gz
RUN cat /root/tigervnc.tar.gz | tar xz --strip 1 -C /

COPY start.sh /root/

# VNC password is 123456
# rebember to start chrome with "--no-sandbox"

CMD /root/start.sh
