#!/bin/bash
set -e

export DEBIAN_FRONTEND=noninteractive

echo "Install vnc and xfce"

apt update
apt install -q -y vim wget net-tools locales bzip2 supervisor xfce4 xfce4-terminal vnc4server libnss-wrapper gettext
apt-get purge -y pm-utils xscreensaver*
apt-get clean -y

echo "generate locales for en_US.UTF-8"
locale-gen en_US.UTF-8
