#!/bin/bash

set -x
#set -e

export VNC_PW="123456"

mkdir -p "$HOME/.vnc"
export PASSWD_PATH="$HOME/.vnc/passwd"

echo "$VNC_PW" | vncpasswd -f >> $PASSWD_PATH
chmod 600 $HOME/.vnc/passwd

echo -e "\n------------------ startup of VNC server ------------------"

echo "Kill old server"
vncserver -kill :1

echo "Start new server"
vncserver :1 -depth 24 -geometry 1280x1024


echo -e "\n------------------ startup of Xfce4 window manager ------------------"

### disable screensaver and power management
xset -dpms &
xset s noblank &
xset s off &

/usr/bin/startxfce4 --replace > $HOME/wm.log &
sleep 1
cat $HOME/wm.log

sleep infinity
